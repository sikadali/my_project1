package net.tncy.sda.myproject1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestCalculator {
    @Test
    void calculate(){
        Calculator c = new Calculator(5, 2);
        Assertions.assertEquals(5/3, c.division());
    }
}
