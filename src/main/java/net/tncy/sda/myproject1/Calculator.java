package net.tncy.sda.myproject1;

public class Calculator {
    private int left;
    private int right;

    public Calculator(int left, int right) {
        this.left = left;
        this.right = right;
    }

    public float division(){
        return this.left / this.right;
    }
}
